<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 06/05/19
 * Time: 22:00
 */

namespace App\Twig;


use App\Utils\DeltaRenderExtension\CodeExtension;
use nadar\quill\Lexer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class QuillRender extends AbstractExtension
{
    public function getFilters()
    {
        return [new TwigFilter('quillRender', [$this, 'quillFilter'])];
    }

    public function quillFilter(string $quillJson)
    {
        $lexer = new Lexer($quillJson);
        $lexer->registerListener(new CodeExtension);
        $lexer->escapeInput = true;
        return $lexer->render();
    }
}