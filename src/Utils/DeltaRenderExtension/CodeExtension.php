<?php
/**
 * Created by PhpStorm.
 * User: kip
 * Date: 16/05/19
 * Time: 23:07
 */

namespace App\Utils\DeltaRenderExtension;


use nadar\quill\BlockListener;
use nadar\quill\Lexer;
use nadar\quill\Line;

class CodeExtension extends BlockListener
{
    /**
     * {@inheritDoc}
     */
    public function process(Line $line)
    {
        if ($line->getAttribute('code-block')) {
            $this->pick($line);
            $line->setDone();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function render(Lexer $lexer)
    {
        foreach ($this->picks() as $pick) {
            // get all
            $prev = $pick->line->previous(function (Line $line) {
                if (!$line->isInline()) {
                    return true;
                }
            });

            // if there is no previous element, we take the same line element.
            if (!$prev) {
                $prev = $pick->line;
            }

            $pick->line->output = '<code>'.$prev->getInput() . $pick->line->renderPrepend() . '</code>';
            $prev->setDone();
        }
    }

}