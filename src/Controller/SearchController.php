<?php

namespace App\Controller;

use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request)
    {
        $query = $request->query->get('query');
        $page = $request->query->get('page', 1);
        $questions = $this->questionRepository->getQuestions(['query' => $query], $page);

        return $this->render('search/index.html.twig', [
            'controller_name' => 'SearchController',
            'questions' => $questions,
        ]);
    }
}
