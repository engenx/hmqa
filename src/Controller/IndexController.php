<?php

namespace App\Controller;

use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    private $questionRepository;

    public function __construct(
        QuestionRepository $questionRepository
    ) {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $pageNumber = $request->query->get('page',1);
        $questions = $this->questionRepository->getQuestions([], $pageNumber);

        return $this->render('index/index.html.twig', [
            'questions' => $questions,
        ]);
    }
}
